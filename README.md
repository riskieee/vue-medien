# vue-medien

[MEDIENkompetenz](http://medien.karpinski.berlin/) 
----
![screenshot](https://digitalpraxis.de/images/biger/projekt_imbb.jpg)

**[DEMO](https://imbb.metavrse.de/)**

MEDIENkompetenz ist eine freie kuratierte Sammlung mit Tutorials, Workshops und Infos für Eltern und Kinder.
MEDIENkompetenz (MEDIAcompetence) is a free curated collection of tutorial, workshops infos for parents and children. 

`Mainly it's an Vue.js based search layer on an JSON import :)`

MEDIENkompetenz is [my](http://digitalpraxis.de/) try to collect resources when using media with my kids.
If you have something to share I missed: contact me by info@metavrse.de
  

### npm

Install via npm:

```bash
// get the git repo 
git clone git@gitlab.com:riskieee/vue-media.git

// get all enpencies
npm install

// start dev server
npm run serve

// start build 
npm run build

// run linter 
npm run lint

```

### comming features
  1. JSON import as component
  2. multiword search + tagging
  3. backend for with DB for content creation and edition

Have FUN!